Hubbub
======
Hubbub is a website showing timeline of commits
###Description
  Showing timeline of commits in your GitHub News Feed
###Requirement
  web browser which can render this web site
###Usage
  access [Hubbub](http://hubbub.giikey.com "Hubbub") and login
###Future
  no idea
###License
  see [License](LICENSE)
###Author
  [ryo33](https://github.com/ryo33/ "ryo33's github page")
